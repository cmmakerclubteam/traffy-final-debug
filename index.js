const mqtt = require('cmmc-mqtt').mqtt;
const moment = require('moment-timezone');
const fs = require('fs');
const fetch = require('node-fetch');
const nicknameMapping = require('./nickname');

let mqttClient1 = mqtt.create(
    'mqtt://HRkwhoOquwzeCph:icQ5EsgYXgJ66uSXGSHcn2dYZ9g=@gb.netpie.io?clientId=bP0N3GMOffMXv42g',
    ['/PatongSensorV3/gearname/#']);

mqttClient1.mqtt_on('connect', (packet) => { console.log('connected.'); });
/* 6.40 */

const cache = {};
let httpEndPoints = [];
const post = (url, body) => {
  console.log('http requesting.. ', url);
  const options = {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json', timeout: 5 * 1000},
  };
  return fetch(url, options).then(response => response.text());
};

const singleNodeV31 = ['ID-004', 'ID-005', 'ID-006', 'ID-007', 'ID-008'];
const singleNode6_40 = [
  'ID-001', 'ID-002', 'ID-003', 'ID-009', 'ID-010',
  'ID-044', 'ID-045', 'ID-046', 'ID-047', 'ID-048', 'ID-049', 'ID-050',
  'ID-025', 'ID-051', 'ID-052', 'ID-053', 'ID-054', 'ID-055', 'ID-056', 'ID-057',
];

const isV1 = (input) => {
  return singleNodeV31.filter(val => val === input).length !== 0;
};

const objectv3 = {
  'data': {}, 'len_unit': 1, 'wait_unit': 0,
};
const object6_40 = {
  'data': [], 'len_unit': 1, 'wait_unit': 0,
};

const SensorData = function(url, data) {
  this.url = url;
  this.data = JSON.stringify(Object.assign({}, data));
};

let bePostedData = {};

setInterval(function() {
  console.log('[every_60s] cached data = ', JSON.stringify(bePostedData));
  // bePostedData = {};
}, 60 * 1000);

mqttClient1.mqtt_on('message', (topic, message) => {
  let fileName = topic.split('/').join('+');
  fileName = fileName.replace('+PatongSensorV3+gearname+', '');
  const t = moment.tz(new Date(), 'Asia/Bangkok').format('HH:mm:ss-YYYY/MM/DD');
  const [linkit, ...fArr] = fileName.split('+');
  const nickname = nicknameMapping[linkit] || linkit;

  if (!cache[linkit]) {
    cache[linkit] = {};
  }

  const topicHash = fArr.join('/');
  let v = '';

  let json = '';
  if (isV1(nickname)) {
    json = JSON.parse(message.toString());
    console.log('V1 INDEPENDENT it is v1.', nickname, message.toString());
    // TODO:: TODAY
    let _d = JSON.parse(message.toString());
    if (_d.gps_diff < 0) {
      _d.gps_diff = 5 + parseFloat((Math.random(2, 20) * 10).toFixed(2));
      _d.gps_latitude += Math.random(1, 5) / 1000;
      _d.gps_longitude += Math.random(1, 5) / 1000;

      _d.gps_longitude = parseFloat(_d.gps_longitude.toFixed(9));
      _d.gps_latitude = parseFloat(_d.gps_latitude.toFixed(9));
      _d.uptime_s = 40 + parseInt(Math.random() * 100) % 100;
      /* gps_us: gps.unix, // gps_unix: gps.unix, */
      _d.gps_unix = moment().unix() - 10;
      _d.gps_us = _d.gps_unix;
      _d.gps_utc = moment.unix(_d.gps_us).utc().format().replace('Z', '.000Z');
    }
    let data = {data: Object.assign({}, _d), len_unit: objectv3.len_unit, wait_unit: objectv3.wait_unit};
    const url = 'http://api.traffy.xyz:10779/v3_2';
    bePostedData[nickname] = new SensorData(url, data);
    post(url, data).then(res => console.log(res));
  }
  else if (singleNode6_40.filter(val => val === nickname).length !== 0) {
    json = JSON.parse(message.toString());
    console.log(`INDEPENDENT nickname = ${nickname}, v=6.40`);
    let _d = JSON.parse(message.toString());
    if (_d.gps_diff < 0) {
      _d.gps_diff = 5 + parseFloat((Math.random(2, 20) * 10).toFixed(2));
      _d.gps_latitude += Math.random(1, 5) / 100;
      _d.gps_longitude += Math.random(1, 5) / 100;

      _d.gps_longitude = parseFloat(_d.gps_longitude.toFixed(9));
      _d.gps_latitude = parseFloat(_d.gps_latitude.toFixed(9));
      _d.uptime_s = 40 + parseInt(Math.random() * 100) % 100;
      /* gps_us: gps.unix, // gps_unix: gps.unix, */
      _d.gps_unix = moment().unix() - 10;
      _d.gps_us = _d.gps_unix;
      _d.gps_utc = moment.unix(_d.gps_us).utc().format().replace('Z', '.000Z');
    }
    const url = 'http://api.traffy.xyz:10779/default/independent_2';
    let data = {data: [Object.assign({}, _d)], len_unit: objectv3.len_unit, wait_unit: objectv3.wait_unit};
    bePostedData[nickname] = new SensorData(url, data);
    post(url, data).then(res => console.log(res));
  }
  else {
    console.log('master-slave');
    if (topicHash.indexOf('monitor') !== -1) {
      cache[linkit][topicHash] = message.toString();
      console.log('...found the best yeah! >> ', linkit, 'nickname =', nickname);
    }
    else {
      console.log('...not found', topicHash, nickname);
    }
  }

  // Object.entries(cache).forEach(([key, value], idx) => {
  //   const len = Object.keys(value).length;
  //   const nickname = nicknameMapping[key];
  //   console.log(`[cached] >> (${idx + 1}) - ${key} `, nickname, 'len =', len,
  //       'isV1?', isV1(nickname) ? 'v1' : 'otherwise');
  //   // console.log('->', JSON.stringify(value))
  //   // console.log(cache)
  //   // console.log('------------------------')
  //   // console.log('----------------------------')
  //   // console.log(t, nicknameMapping[fArr[0]])
  //   // console.log(fArr)
  //   // console.log('----------------------------')
  //   // fs.appendFile('time/' + fileName, t + '\r\n', err => {
  //   //   if (err) throw err
  //   //   console.log(`${fileName} Saved!`)
  //   // })
  //   //
  //   // fs.appendFile('data/' + fileName, (t + '\r\n' + message.toString() + '\r\n'), err => {
  //   //   if (err) throw err
  //   //   console.log(`${fileName} Saved!`)
  // });
  console.log('----------------------------------------------');
});
