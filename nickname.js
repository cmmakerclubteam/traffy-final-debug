const nicknameMapping = {
  '5ccf7f094e69': 'Patong1',
  '1afe34daf20a': 'Patong1_node1',
  '1afe34daf24f': 'Patong1_node2',
  '5ecf7f094e04': 'Patong1_node3',
  '5ecf7f096118': 'Patong1_node4',
  '18fe34eecd4f': 'Patong2',
  '1afe34db3d1c': 'Patong2_node1',
  '5ecf7f0951b6': 'Patong2_node2',
  '5ecf7f09cbcc': 'Patong2_node3',
  '5ecf7f099c73': 'Patong2_node4',
  '5ccf7f094e77': 'Patong3',
  '1afe34ee7f63': 'Patong3_node1',
  '5ecf7f094e1d': 'Patong3_node2',
  '1afe34db3a75': 'Patong3_node3',
  '5ecf7f0950a4': 'Patong3_node4',
  '5ccf7f0950b3': 'Patong4',
  '1afe34daf17e': 'Patong4_node1',
  '5ecf7f09cc09': 'Patong4_node2',
  '1afe34eea100': 'Patong4_node3',
  '5ecf7f09520e': 'Patong4_node4',
  /* start v3 */
  '9c65f920c20d': 'ID-001',
  '9c65f920a570': 'ID-001', /* replace */
  '9c65f920b167': 'ID-002', /* replaced to b140 */
  '9c65f920c213': 'ID-003',
  '9c65f920a945': 'ID-003', /* replace */
  '9c65f920c205': 'ID-004',
  '9c65f920a996': 'ID-005',
  '9c65f920c1e5': 'ID-006',
  '9c65f920aa4a': 'ID-007',
  '9c65f920c212': 'ID-008',
  // '9c65f920b08b': 'ID-009', /* replaced with b08b */
  '9c65f9209f3a': 'ID-009', /* replaced with b08b */
  '9c65f920b140': 'ID-025', /* replace c2a4 */
  '9c65f920a596': 'ID-012', /* _M002 */
  '9c65f9209fb5': 'ID-013', /* _M003 */
  '9c65f920a998': 'ID-014', /* _M004 */
  '9c65f920c277': 'ID-015', /* _M005 */
  '9c65f9209f01': 'ID-016', /* _M006 */
  '9c65f920b155': 'ID-017', /* _M007 */
  '9c65f920b12c': 'ID-018', /* _M008 */
  '9c65f920b171': 'ID-019',
  '9c65f920a5a1': 'ID-011',  // nectec
  '9c65f9209f69': 'ID-020', // nectec
  '9c65f920a574': 'ID-027', // testing cnx
  '9c65f920acbd': 'ID-021',
  '9c65f920b923': 'ID-022',
  '9c65f920a5a2': 'ID-023',
  '9c65f920ac5d': 'ID-024', /* _M014 */ /* replaced */
  '9c65f920a12d': 'ID-010',
  '9c65f920b139': 'ID-044',
  '9c65f920a983': 'ID-045',
  '9c65f920aa46': 'ID-046',
  '9c65f920a9c8': 'ID-047',
  '9c65f920b14b': 'ID-048',
  '9c65f920c209': 'ID-049',
  '9c65f920c270': 'ID-050',
  // '9c65f920a9bb': 'ID-051', /* die */
  '9c65f920c214': 'ID-051',
  '9c65f920b172': 'ID-052',
  // '9c65f920a9aa': 'ID-053', /* die */
  '9c65f920c2a4': 'ID-053',
  '9c65f920b4a6': 'ID-054',
  '9c65f920b9f9': 'ID-055',
  '9c65f920a18a': 'ID-056',
  '9c65f920a9ad': 'ID-057',
}

module.exports = nicknameMapping
