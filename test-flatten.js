var flat = require('flat');
var a = require('./v640_ID-001_0926_13-10-21');
var b = require('./v640_ID-001_0926_13-10-32');
var c = require('./v640_ID-001_0926_13-20-21');

let check = function(a, b) {
  let ct = 0;
  let same = 0;
  Object.entries(a).forEach(([key, val]) => {
    if (a[key] === b[key]) {
      same++;
    }
    ct++;
  });
  return {
    total: ct,
    same,
    percent: (same / ct * 100),
  };
};

let isSameObjectData = (aa, bb) => {
  let _a = flat(aa);
  let _b = flat(bb);
  let result = check(_a, _b);
  console.log(result);
  return result.percent === 100;
};

console.log(`isSameObject=${isSameObjectData(a, b)}`);
console.log(`isSameObject=${isSameObjectData(a, c)}`);
// console.log(check(a1_f, a2_f));

