const json = require('./v3_ID-005')
const dummy = require('./dummy')
const moment = require('moment-timezone')

const fetch = require('node-fetch')
const post = (url, body) => {
  console.log('http requesting.. ', url)
  const options = {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json', timeout: 5 * 1000}
  }
  return fetch(url, options).then(response => response.text())
}

///const p4master = require('./v2_master+5ccf7f0950b3+status')
///const p4n1 = require('./v2_client+5ccf7f0950b3+Patong4_node1+status')
///const p4n2 = require('./v2_client+5ccf7f0950b3+Patong4_node2+status')
///const p4n3 = require('./v2_client+5ccf7f0950b3+Patong4_node3+status')
///const p4n4 = require('./v2_client+5ccf7f0950b3+Patong4_node4+status')

//p4master._time = moment().unix()
//p4n1._time = moment().unix()
//p4n2._time = moment().unix()
//p4n3._time = moment().unix()
//p4n4._time = moment().unix()
//
//post('http://api.traffy.xyz:10779/v2', p4master).then(res => console.log('v2 speccifica res=', res))
//post('http://api.traffy.xyz:10779/v2', p4n1).then(res => console.log('v2 speccifica res=', res))
//post('http://api.traffy.xyz:10779/v2', p4n2).then(res => console.log('v2 speccifica res=', res))
//post('http://api.traffy.xyz:10779/v2', p4n3).then(res => console.log('v2 speccifica res=', res))
//post('http://api.traffy.xyz:10779/v2', p4n4).then(res => console.log('v2 speccifica res=', res))

const id004 = require('./v3_ID-004')
id004.unix = moment().unix()
id004.gps_us = id004.unix
id004.utc = moment.unix(id004.unix).utc().format().replace('Z', '.000Z')
console.log(id004)
post('http://api.traffy.xyz:10779/v3', {
  data: id004,
  len_unit: 1,
  wait_unit: 0
}).then(res => console.log('v3 speccifica res=', res))
