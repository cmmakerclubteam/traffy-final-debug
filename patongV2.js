const mqtt = require('cmmc-mqtt').mqtt
const moment = require('moment-timezone')
const fs = require('fs')
const fetch = require('node-fetch')
const nicknameMapping = require('./nickname')

let mqttClient1 = mqtt.create('mqtt://mqtt.cmmc.io', [
  'WORK/TRAFFY_V2/+/+/status', 'WORK/TRAFFY_V2/+/+/+/status'])

mqttClient1.mqtt_on('connect', (packet) => {
  console.log('connected.')
})

mqttClient1.mqtt_on('message', (topic, message) => {
  const json = JSON.parse(message.toString())
  json.pretty_time = moment.unix(json._time / 1000).format()
  console.log(topic, message.toString())
  let fileName = topic.split('/').join('+')
  fileName = fileName.replace('WORK+TRAFFY_V2+', '')
  console.log(`writing ${fileName}.json`)
  fs.writeFileSync(`v2_${fileName}.json`, JSON.stringify(json, null, 4)) 
})
