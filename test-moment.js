var data = require('./data.json');
var moment = require('moment-timezone');

console.log(data);

data.forEach(function(item) {
  var timeString = moment.unix(item.unix_time);
  console.log(`distance=${item.distance_cm}, time=${timeString}`);
});
